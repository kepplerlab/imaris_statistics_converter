"""gets folders in 'input' and edits and merges all files therein"""
import os
import legacy_process as p

#setup folder directories
in_folder = 'input'
out_folder = 'output/'
dump_folder = 'processed_original_files/'
csv_dir = 'input'
file_dic = {}
lablel_archive = {}
convert_dic = {}
keep_criteria = ['Area', 'Median', 'Position', 'Shortest Distance', 
                'Volume', 'Sphericity']
intensity_types = ['Median', 'Mean', 'Center', 'Max', 'Min']

def create_files_dict(in_dir=csv_dir):
    """returns dictionary of folders and files in "input" folder"""
    #creates a list of all the folders
    folder_list = []
    with os.scandir(in_dir) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                folder_list.append(entry.name)

    #creates a dictionary with lists of the files in these folders
    global file_dic
    for folder in folder_list:
        folder_dir = in_dir + '/' + folder
        files = []
        for file in os.listdir(folder_dir):
            files.append(file)
        #only select csv files and write them in a list
        csv_list = []
        for file in files:
            if file.endswith('.csv'):
                csv_list.append(file)
        #associate list with folder if there are csv files
        if csv_list:
            file_dic[folder] = csv_list

def list_comparison(sample, criteria):
    """screens two lists for a shared item and returns a flag and the item as file label"""
    #make a list of shared elements in two input lists
    shared = [s for s in sample and criteria if s in sample and criteria]

    #check whether a list was created and if so how long it is
    #if 
    if shared and len(shared) == 1:
        if shared[0] in intensity_types:
            channel = sample[sample.index(shared[0])+1]
            label = str(shared[0] + ' ' + channel)
        else:
            label = shared[0]
        return True, label
    #raise warning regarding confusing file naming 
    elif shared and len(shared) > 1:
        print("File name contains mutliple parameter names.")
    else:
        return False, None

def get_file_labels():
    """returns a list of available file labels"""
    global file_dic
    global lablel_archive
    for folder, files in file_dic.items():
        file_labels = []
        for file in files:
            #make file name short and split its parts into a list
            short_name = file.rstrip('.csv')
            parts = short_name.split('_')
            #assuming the folder name is part of the file names:
            #determine how many words those are and slice the list of the files to remove them
            x = len(folder.rstrip('Statistics').split('_'))-1
            parts = parts[x:]
            #reform string label from list w/ some special cases for better readability
            if parts[0] == 'Intensity':
                label = str(parts[1] + ' ' + parts[2])
            elif parts[0] == 'Number':
                label = str(parts[0] + ' ' + parts[2])
            elif parts[0] == 'Ellipticity':
                label = str(parts[0] + ' ' + parts[1])
            elif parts[0] == 'Shortest':
                label = str(parts[0] + ' ' + parts[1] + ' ' + parts[4])
            else:
                label = str(parts[0])
            file_labels.append(label)
        lablel_archive[folder] = file_labels
    return lablel_archive


def kill_files(selectors=keep_criteria):
    """write a new file_dictionary without unnecesary files"""
    global convert_dic
    global file_dic
    for folder, files in file_dic.items():
        convert_files = []
        file_labels = []
        for file in files:
            name_parts = file.rstrip('.csv').split("_")
            flag, label = list_comparison(name_parts, selectors)
            if flag:
                convert_files.append(file)
                file_labels.append(label)
        folder_dic = {
            'size': len(convert_files),
            'labels': file_labels,
            'files': convert_files
        }
        convert_dic[folder] = folder_dic

def concat_folder(file, folder, file_id, concat_data):
    """concatenates all edited files of a batch folder"""
    file_dir = out_folder + folder
    file_data = p.csv_to_list(file, file_dir)
    concat_data = p.edit_data(file_data, file_id, concat_data)
    return concat_data

def cycle_batch():
    """edits and concatenates all csv files of each folders"""
    create_files_dict()

    #cycles through all the folders for file editing
    for folder in file_dic.keys():
        folder_dir = in_folder + '/' + folder
        out_dir = out_folder + folder + '/'
        dump_dir = dump_folder + folder
        try:
            os.mkdir(f"{out_folder}{folder}")
        except FileExistsError:
            print(f"The folder '{folder}' was already processed before.")
        file_ids = p.edit_files(file_dic[folder], folder_dir, out_dir, dump_dir)
        #create a concatenated file
        concat_list = []
        for file in file_dic[folder]:
            file_id = file_ids[file]
            filename = file.rstrip('.csv') + '_cut.csv'
            concat_list = concat_folder(filename, folder, file_id, concat_list)
        concat_name = out_folder + folder + '_cut.csv'
        p.list_to_csv(concat_list, concat_name)