"""tools to edit csv files in 'input'"""
import os
import csv

#setup folder directories
in_folder = 'input'
out_folder = 'output/'
dump_folder = 'processed_original_files/'

#define header specific parameters to keep; start counting  from '0'!
instructions = {'Area': [0], 'Intensity Median': [0], 'Position': [0, 1, 2, 7],
                'Shortest Distance': [0], 'Ellipticity (oblate)': [0], 'Ellipticity (prolate)': [0],
                'Volume': [0], 'Sphericity': [0]}
set_ch_to = ''


def files_list():
    """returns list of files in "to_do" folder"""
    files = []
    for file in os.listdir(in_folder):
        files.append(file)
    #only select csv files
    csv_list = []
    for file in files:
        if file.endswith('.csv'):
            csv_list.append(file)
    return csv_list

def csv_to_list(name, folder_dir=in_folder):
    """reads all rows of a csv file into a list and returns it"""
    rows = []
    filename = f"{folder_dir}/{name}"
    with open(filename, newline='') as f:
        rows = list(csv.reader(f))
    return rows

def get_edit_parameters(rows):
    """get editing parameters depending on metric"""
    #sets the descriptive second line as string and splits it
    metric = ''.join(str(e) for e in rows[1])
    description = metric.split()
    #creates 'metric' key and searches in lookup dictionary
    metric = ' '.join(map(str, description[:2]))
    if metric in instructions.keys():
        indentifier = instructions[metric]
        #sets variable to improve column header of fluorescenes data
        if metric == 'Intensity Median':
            global set_ch_to
            set_ch_to = description[2]
        return indentifier
    else:
        return []

def cut_list(listed):
    """removes first three list elements"""
    short_list = listed[3:]
    return short_list

def edit_data(data, indentifier, existing_data=[]):
    """removes unnecessary elements depending on metric"""
    if indentifier:
        y = 0
        while y < len(data):
            content = data[y]
            #wipes row #y to replace it with the shortened version
            #for batch processing: data is replaced with concat data
            if existing_data != []:
                data[y] = existing_data[y]
            else:
                data[y] = []
            for x in indentifier:
                data[y].append(str(content[x]))
            y += 1
        return data
    #returns existing data without adding unidentified data
    elif existing_data != []:
        return existing_data
    else:
        return data

def rename_channel_columns(data, set_ch_to):
    """renames column header"""
    headers = data[0]
    data[0] = []
    for header in headers:
        if header == 'Intensity Median':
            data[0].append(set_ch_to)
        else:
            data[0].append(header)
    return data

def list_to_csv(rows, new_file):
    """writes lists into a specially named csv file"""
    try:
        with open(new_file, 'w', newline='') as csv_f:
            content = csv.writer(csv_f, delimiter=',', quoting=csv.QUOTE_MINIMAL, escapechar='')
            for row in rows:
                content.writerow(row)
    except PermissionError:
        exception_raised = True
        print(f"A file named '{new_file.lstrip('output/')}' has already been created before.")
        print(f"Please empty the 'output' folder or rename your file!")
        return exception_raised

def make_dump_dir(dump_dir):
    """creates needed folders for processed originals"""
    try:
        os.mkdir(dump_dir)
    except FileExistsError:
        pass

def move_originals(element, element_dir=in_folder, dump_dir=dump_folder):
    """moves originals in new folder"""
    #rename() can't replace existing files. No accidental deleting
    if dump_dir != dump_folder:
        make_dump_dir(dump_dir) 
    try:
        os.rename(f"{element_dir}/{element}", f"{dump_dir}/{element}")
    except FileExistsError:
        print(f"A file named '{element}' has already been processed before.")
        print(f"Please empty the corresponding directory or rename your file!")
    except PermissionError:
         print(f"The file named '{element}' is currently in use by another program.")

def edit_files(files=files_list(), folder_dir=in_folder, out_dir=out_folder, dump_dir=dump_folder):
    """executes editing process"""
    file_ids = {}
    for file in files:
        #transform into list of rows called 'rows'
        rows = csv_to_list(file, folder_dir)

        #obtain and execute editing
        global file_id
        file_id = get_edit_parameters(rows)
        rows = edit_data(cut_list(rows), file_id)

        #check if to concatenate and ...
        #[OPTIONAL] rename column of fluorescence channels
        if set_ch_to != '':
            rows = rename_channel_columns(rows, set_ch_to)

        #create a newly named csv file with the results
        new_file = out_dir + file.rstrip('.csv') + '_cut.csv'
        exception_raised = list_to_csv(rows, new_file)

        #move the processed files to the 'processed' folder but check
        #for file creation exception of list to csv first
        if not exception_raised == True:
            move_originals(file, folder_dir, dump_dir)
        columns = []
        for x in range(len(file_id)):
            columns.append(x)
        file_ids[file] = columns 
    return file_ids