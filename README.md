# imaris_statistics_converter
standalone converter for Imaris (Bitplane) statistics exports into FlowJo-compatible csv or Excel file

#
Hofmann J, Gadjalova I, Mishra R, Ruland J and Keppler SJ (2021) **Efficient Tissue Clearing and Multi-Organ Volumetric Imaging Enable Quantitative Visualization of Sparse Immune Cell Populations During Inflammation.** _Front. Immunol._ 11:599495.
https://www.frontiersin.org/articles/10.3389/fimmu.2020.599495/full	
#

25-06-12,021	v0.5.2

removes first three lines ontop of the csv files //
keeps only the useful colums (takes 'ID' column from "...area.csv") //
improves column headers for readability //
concatenates edited files into on FlowJo compatible csv named as '{folder}_cut.py' //
#
installation:

download executable
#
usage instructions:

  - create objects and 'Export All Statistics to file' in Imaris
  - execute application
  - select folder containing the data folders
  - all edited and the concatenated files can be found in the 'output' folder
  - repeated execution will overwrite the summary file
  - [csv files] drag and drop resulting files into FlowJo (autogenerates .fcs file)
  - empty the folders every now and then to avoid raising file naming issues
#
pandas (https://pandas.pydata.org) module is used for easy data handling //
tkinter module is used for the graphical user interface
