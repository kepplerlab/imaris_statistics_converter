"""edits all files in the 'input' folder depending on selection"""
import conversion_gui as cg
import json

#complex approach for customizations
class GUI():
    """GUI instance"""
    def __init__(self, debug=False):
        if debug:
            self.debug_start()
        else:
            self.start()

    def start(self):
        """"start up and run program widget and save user prefences"""
        #debug!!! calls last directory
        last_directory, parameter = cg.run_program()
        self.save_preferences(last_directory, parameter)

    def debug_start(self):
        """"start up and run program widget and save user prefences"""
        #debug!!! calls last directory
        directory = self.call_directory()
        last_directory, parameter = cg.run_program(directory)
        self.save_preferences(last_directory, parameter)

    def save_preferences(self, directory, parameter):
        """save user preferences as json"""
        presets = {'directory': directory, 'parameter': parameter}
        with open('preferences.json', 'w') as json_file:
            presets_json = json.dump(presets, json_file, indent = 4,
                                     sort_keys=True)

    def call_directory(self):
        with open('preferences.json') as json_file:
            data = json.load(json_file)
        return data['directory']
        

#create active instance
app = GUI()