"""user interface for csv converions"""
import tkinter as tk
from tkinter import filedialog as fd
from tkinter import messagebox as mb
import batch_process as bp
from batch_process import cycle_batch
from pathlib import Path

#dictionary of classic stats available
options = {0: 'Area',1: 'Median',2: 'Position',3: 'Shortest',
           4: 'Volume',5: 'Sphericity'}

#create and name main window framework
main_app = tk.Tk()
main_app.resizable(True, False)
main_app.geometry('450x350')
main_app.title("Imaris to FlowJo / Excel")

#create main building blocks
main_frame = tk.Frame(main_app)
sub_frame = tk.Frame(main_frame)
choice_frame = tk.Frame(main_frame)
box = tk.Listbox(choice_frame, selectmode='extended')
choice = tk.IntVar(choice_frame)

#parameter setup
items = []
selected_items = []
font = ('Helevetica 10')
font_bold = ('Helevetica 9 bold')
base_dir = Path()
directory = Path()
current_dir = tk.StringVar(main_app)

#define general action button class
class PushButton(tk.Button):
    def __init__(self, text, bg, command):
        tk.Button.__init__(
            self, sub_frame, width=20, height=2, justify='center', bd=2,
            font=font_bold, text=text, bg=bg, command=command)

#define commands necessary
def call_dir():
    """freshly calls current dir"""
    directory = Path(current_dir.get())
    return directory

def select_dir():
    """selecet a current directory"""
    selected_dir = Path(fd.askdirectory(title='Select your data folder',
                                        initialdir=current_dir.get()))
    if selected_dir.exists() and selected_dir.is_dir():
        current_dir.set(f'{selected_dir}')
    else:
        msg = 'An error occured during directory selection. Retry'
        mb.showinfo(title='Selection Error', message=msg)

def confirm_button(multiple=False):
    """build confirmation button"""
    pady = 10
    b_txt_4 = 'Confirm Selection'
    confirm = tk.Button(choice_frame, width=15, justify='center', bd=2,
                        font=font_bold, text=b_txt_4, bg='#56B4E9',
                        command=get_choice)
    if multiple:
        pady = 5
        confirm.configure(command=get_selection)
    confirm.grid(padx=5, pady=5, sticky='w')
    leave.grid(pady=pady)

def build_listbox():
    """create listboxes for every available stat of every folder"""
    temp = {}
    temp = bp.get_file_labels(bp.create_files_dict(call_dir(), temp))
    #make a comprehensive list of all options in all folders
    global items
    for x in temp.keys():   
        items = items + temp[x]
    items = list(dict.fromkeys(items))
    #create the listbox with the items/options
    for item in items:
        box.insert('end', item)
    box.grid(row=1, column=0, padx=5)
    #confirmation button
    confirm_button(1)

def build_radios(v, choices=options):
    """create a radiobutton for every available stat option"""
    for num, option in choices.items():
        tk.Radiobutton(choice_frame, text=option, variable=v, value=num,
                       font=font).grid(row=num, sticky='w')
    #confirmation button
    confirm_button()

def prep_choice(multiple=False):
    """prep for user selection"""
    sub_frame.pack_forget()
    if multiple:
        new_intro = "Select which statistic(s) should be exported:"
        build_listbox()
    else:
        new_intro = "Select which statistic should be exported:"
        build_radios(choice)
    intro.configure(text=new_intro)
    choice_frame.pack(fill='both')

def multi_opt():
    """allows multiple user selections"""
    prep_choice(True)

#selection returning code
def get_selection():
    """gets user selections"""
    selected = list(box.curselection())
    for item in selected:
        selected_items.append(items[item])    
    run_custom(selected_items)

def get_choice():
    """gets user selection"""
    run_a_stat(choice.get())

#commence data manipulation
def run_default():
    """run default csv batch processing for FlowJo"""
    cycle_batch(call_dir())
    exit()

def run_custom(selection):
    """run csv batch processing for FlowJo for custom selection"""
    cycle_batch(call_dir(), 3, selection)
    exit()

def run_a_stat(choice):
    """run version of batch processing that generates excel file of one stat"""
    selected_items.append(options[choice])
    cycle_batch(call_dir(), 2, selected_items)
    exit()

def run_filaments():
    """run filament adapted version of batch processing"""
    cycle_batch(call_dir(), 1, ['Dendrite', '(sum)', 'Branch', 'mean'])
    exit()

# def recover():
#    """close application"""
#    choice_frame.pack_forget()
#    back.grid_forget()
#    sub_frame.pack()

def exit():
    """close application"""
    main_app.destroy()

#general elements
text = "Convert exported Imaris statistics to ..."
dir_text = f"selected input directory:"
intro = tk.Label(main_app, text=text, font=('Helevetica 12 bold'))
dir_select = tk.Label(main_app, text=dir_text, font=('Helevetica 10 italic'))
selection = tk.Label(main_app, textvariable=current_dir, font=font)
select = tk.Button(main_app, width=6, text='change', bg='#CC79A7',
                  command=select_dir)
leave = tk.Button(main_app, width=6, text='quit', bg='#D55E00',
                  command=exit)
#back = tk.Button(main_app, width=4, text='back', bg='#F0E442', command=recover)
#back.grid(row=2, column=1, padx=10, pady=pady, sticky='es')

#the explanatory main text labels
csv_txt = f" ... FlowJo compatible csv files"
xlsx_txt = f"\n... simple excel files of"
csv_label = tk.Label(sub_frame, text=csv_txt, font=font)
xlsx_label = tk.Label(sub_frame, text=xlsx_txt, font=font)

#the main execution buttons
b_txt_1 = 'surface(s) / spot(s)'
surf_spots = PushButton(text=b_txt_1, bg='#0072B2', command=run_default)
b_txt_2 = 'w/ custom settings'
custom = PushButton(text=b_txt_2, bg='#F0E442', command=multi_opt)
b_txt_3 = 'A. a single statistic'
one_stat = PushButton(text=b_txt_3, bg='#56B4E9', command=prep_choice)
b_txt_4 = 'B. filaments'
filaments = PushButton(text=b_txt_4, bg='#009E73', command=run_filaments)

#place general stuff
intro.grid(row=0, column=0, padx=5, pady=15, sticky='nw')
dir_select.grid(row=1, column=0, padx=5, pady=0, sticky='nw')
selection.grid(row=2, column=0, padx=5, pady=5, sticky='nw')
select.grid(row=2, column=2, sticky='es')
main_frame.grid(row=3, column=0, padx=20, sticky='nw')
leave.grid(row=4, column=2, pady=20, sticky='es')

#place main menu elements
sub_frame.pack(fill='both')
csv_label.grid(row=0, column=0, pady=10, sticky='nw')
surf_spots.grid(row=1, column=0)
custom.grid(row=1, column=1)
xlsx_label.grid(row=2, column=0, pady=10, sticky='nw')
one_stat.grid(row=3, column=0)
filaments.grid(row=3, column=1)

#initiate window loop
def run_program(init_dir=Path.home(), custom_criteria={}):
    """starts up the GUI and returns prefences"""
    base_dir = init_dir
    directory = init_dir
    current_dir.set(f'{directory}')
    main_app.mainloop()
    return current_dir.get(), selected_items