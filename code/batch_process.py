"""gets folders in 'input' and edits and merges all files therein"""
import process as p
from pathlib import Path

#setup default folder directories
surfspot_in_dir = Path('./input_surfaces_spots')
filament_in_dir = Path('./input_filaments')
out_dir = Path('./output')
file_dic = {}
convert_dic = {}
keep_criteria = ['Area', 'Median', 'Position', 'Shortest', 'Volume',
                 'Sphericity']

#datatypes: 
#   0 [default]     convert predefined set of stats to csv and also concatencat them for FlowJo
#   1 [filaments]   convert and concatenate a set of filament stats to one excel file
#   2 [single stat] convert one user-selected surface/spot stat to an excel file
#   3 [custom]      like 0 but with custom selection of keep_criteria

def create_files_dict(in_dir=surfspot_in_dir, file_dictionary=file_dic):
    """returns dictionary of folders and files in "input" folder"""
    #create a list of all folders in initial folder (incl. the latter)
    folder_list = [x for x in in_dir.iterdir() if x.is_dir() and x.stem != 'output']
    folder_list.insert(-1, in_dir)
    #fill file dictionary with all csv files in each directory
    for folder in folder_list:
        folder, csv_files = p.list_files(folder)
        file_dictionary[folder] = csv_files
    #provision for methods who need to call up the dictionary seperatly
    if not file_dictionary == file_dic:
        return file_dictionary

def get_file_labels(file_dic, datatype=0):
    """returns an archive of available file labels"""
    label_archive = {}
    for folder, files in file_dic.items():
        #extract actual file names from a list of file paths
        csv_list = [file.stem for file in files]
        #make file name short, split its parts into a list and get label  
        labels = [p.label(file.split('_'), folder.name) for file in csv_list]
        label_archive[folder] = labels
    return label_archive

def kill_files(selectors, datatype=0):
    """write a new file_dictionary without unwanted files"""
    global convert_dic
    global file_dic
    for folder, files in file_dic.items():
        convert_files = []
        file_labels = []
        for file in files:
            tag = file.stem.split("_")
            if datatype == 3:
                label = p.label(tag, folder.name, datatype)
                flag = p.list_comparison(label, selectors, datatype)
            else:
               flag = p.list_comparison(tag, selectors, datatype)
            if flag:
                convert_files.append(file)
                label = p.label(tag, folder.name, datatype)
                file_labels.append(label)
        #skip writing folders without desired files
        if len(convert_files) == 0:
            continue
        folder_dic = {
            'size': len(convert_files),
            'labels': file_labels,
            'files': convert_files
        }
        convert_dic[folder] = folder_dic

def prep_batch(datatype, selectors, in_dir):
    """preps batch data"""
    #makes a dictionary with all the infos necessary
    create_files_dict(in_dir)
    kill_files(selectors, datatype)

def concat_folder(frames, labels):
    """concatenates all edited files of a batch folder"""
    data = frames['data']
    data.reverse()
    labels.reverse()
    data = p.reoder(data, labels)
    collection = data[0]
    for file in data[1:]:
        #actual concatentation but the order in frames get's flipped
        collection = p.concat_data_frames(file, collection, True)
    return collection

def make_out_dir(in_dir):
    """creates dedicated folders in output directory for processed files"""
    try:
        out_dir = in_dir / 'output'
        out_dir.mkdir()
    except FileExistsError:
        pass
    return out_dir

def make_out_folder(datatype, out_dir, folder):
    """creates dedicated folders in output directory for processed files"""
    if datatype in [0, 3] and folder.stem != 'output':
        try:
            out_folder = out_dir / folder.name
            out_folder.mkdir()
        except FileExistsError:
            pass


#rework selectors into dictionary: 'folder': [selectors, ...]
def cycle_batch(in_dir=surfspot_in_dir, datatype=0, selectors=keep_criteria):
    """edits and/or concatenates the files of each folder according to datatype"""
    #adjusts input directory for filaments if default directories in use
    if datatype == 1 and in_dir == surfspot_in_dir:
        in_dir = filament_in_dir
    #create compendium of files to be edited [= convert_dic]
    prep_batch(datatype, selectors, in_dir)
    #create output directory when not running in default folder system
    if in_dir != surfspot_in_dir or in_dir != filament_in_dir:
        out_dir = make_out_dir(in_dir)
    #cycles through all the folders for file editing
    for folder in convert_dic.keys():
        #setup arguments
        folder_dic = convert_dic[folder]
        files = folder_dic['files']
        labels = folder_dic['labels']
        #create folder for processed files
        make_out_folder(datatype, out_dir, folder)
        if folder == in_dir and not files:
            not_input = False
        elif datatype == 2:
            not_input = False
        else:
            not_input = True
        #run actual data handling section
        frames = p.edit_files(folder, datatype, files, labels, out_dir, not_input)
        #create a concatenated/summary file [if required]
        if datatype in [0, 1, 3]:
            concat_data = concat_folder(frames, labels)
            concat_name = folder.name + '_cut'
            concat_path = out_dir / concat_name
            p.collapse_frame_to_file(concat_data, concat_path, datatype)