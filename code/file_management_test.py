from pathlib import Path
from tkinter import filedialog as fd
from tkinter import messagebox as mb
from pandas import DataFrame, read_csv
import process as p 

import tkinter as tk

base_dir = Path.home()
destination = Path()
filetypes = (('csv files', '*.csv'), ('All files', '*.*'))

'''
#test section
filename = Path("source_data/text_files/raw_data.txt")

print(filename.name)
# prints "raw_data.txt"

print(filename.suffix)
# prints "txt"

print(filename.stem)
# prints "raw_data"

if not filename.exists():
    print("Oops, file doesn't exist!")
else:
    print("Yay, the file exists!")
'''

# add option to rename files if error is reported

def select_dir(base_dir=base_dir):
    directory = Path(fd.askdirectory(title='Select your data folder',
                                     initialdir=base_dir))
    if directory.exists():
        mb.showinfo(title='Selected folder', message=directory)
        return directory
    else:
        print("ERROR - selcetion does not exist")

def select_files(filetypes=filetypes):
    filetypes = filetypes
    filenames = fd.askopenfilenames(title='Open files',initialdir='/',
                                    filetypes=filetypes)
    if filenames:
        mb.showinfo(title='Selected files', message=filenames)
    else:
        print("return to selcetion")

def list_files(folder):
    """returns list of csv files in "input" folder"""
    file_list = sorted(folder.glob('*.csv'))
    return folder, file_list

def frame_up_csv(path):
    """uses pandas to read a csv file into a DataFrame"""
    df = read_csv(path, header=None, skiprows=4, float_precision='high')
    return df

p.edit_files()