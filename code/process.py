"""tools to edit csv files in 'input'"""
from pathlib import Path
from pandas import DataFrame, read_csv, concat
import xlsxwriter

#setup folder directories
in_folder = Path('./input_surfaces_spots')
filament_in_folder = Path('./input_filaments')
out_folder = Path('./output')
intens_typ = ['Median', 'Mean', 'Center', 'Max', 'Min']

#datatypes: 
#   0 [default]     convert predefined set of stats to csv and also concatencat them for FlowJo
#   1 [filaments]   convert and concatenate a set of filament stats to one excel file
#   2 [single stat] convert one user-selected surface/spot stat to an excel file
#   3 [custom]      like 0 but with custom selection of keep_criteria

def list_files(folder=in_folder):
    """returns list of csv files in "input" folder"""
    file_list = sorted(folder.glob('*.csv'))
    return folder, file_list

def list_comparison(sample, criteria, datatype=0):
    """screens two lists for a shared item and returns a flag and the item as file label"""
    #make a list of shared elements in two input lists
    shared = [s for s in sample and criteria if s in sample and criteria]
    #check whether a list was created and if so how long it is 
    if shared and len(shared) == 1 and datatype in [0, 2, 3]:
        return True
    #adapt for complex filament files
    elif shared and len(shared) == 2 and datatype in [1]:
        return True
    #raise warning regarding confusing file naming 
    elif shared and len(shared) > 1:
        print("File name contains mutliple parameter names.")
    else:
        return False

def label(listing, folder_name=None, datatype=0):
    """makes labels out of list with readability in mind"""
    if folder_name:
        #assuming the folder name is part of the file names:
        #determine how many words those are and slice the list of the files to remove them
        x = len(folder_name.rstrip('Statistics').split('_')) - 1
        listing = listing[x:]
    #actual label making
    if listing[-1] == 'cut':
        label = 'x'
    elif listing[0] == 'Intensity' and datatype != 1:
        label = str(listing[1] + ' ' + listing[2])
    elif listing[0] == 'Number' and datatype != 1:
        label = str(listing[0] + ' ' + listing[2])
    elif listing[0] == 'Ellipticity' and datatype != 1:
        label = str(listing[0] + ' ' + listing[1])
    elif listing[0] == 'Shortest' and datatype != 1:
        label = str(listing[0] + ' ' + listing[1] + ' ' + listing[4])
    elif datatype == 1:
        label = str(listing[1] + ' ' + listing[2] + ' ' + listing[3])
    else:
        label = str(listing[0])
    return label

def is_default(folder, datatype, in_folder=in_folder, default=False):
    """adapt for datatype and default folder settings"""
    if folder == in_folder and datatype == 1:
        folder = filament_in_folder
        default = True
    elif folder == in_folder:
        default = True
    return default

def frame_up_csv(path):
    """uses pandas to read a csv file into a DataFrame"""
    try:
        df = read_csv(path, header=None, skiprows=4, float_precision='high')
    except:
        df = DataFrame()
    return df

def get_labels(folder, files):
    """attribute labels to a csv file"""
    #extract actual file names from a list of file paths
    csv_list = [file.stem for file in files]
    #make file name short, split its parts into a list and get label  
    labels = [label(file.split('_'), folder.name) for file in csv_list]
    return labels

def edit_data_frame(data, label):
    """reduce the columns in the frame to be added according to the label"""
    if label == 'Position':
        data = data.drop(columns=[3, 4, 5, 6, 8])
        data.rename(columns={0: 'Position X',1: 'Position Y',
                             2: 'Position Z',7: 'ID'}, inplace=True)
    else:
        x = len(data.columns)
        dispose = list(range(1, x))
        data = data.drop(dispose, axis='columns')
        data.rename(columns={0: label}, inplace=True)     
    return data

def reoder(data, labels, position_flag=False):
    """orders data for concatenation"""
    index = 0
    n_frames = []
    n_lables = []
    for label in labels:
        ll = label.split()
        if label == 'Position':
            pos_data = data[index]
            position_flag = True
        elif 'Distance' in ll or list_comparison(ll, intens_typ):
            n_frames.append(data[index])
            n_lables.append(labels[index])
        else:
            n_frames.insert(0, data[index])
            n_lables.insert(0, labels[index]) 
        index = index + 1
    if position_flag:
        n_frames.append(pos_data)
        n_lables.append('Position')
    return n_frames

def concat_data_frames(add_data, all_data, flip=False):
    """concatenate two DataFrames"""
    if not flip:
        #all_data + add_data
        all_data = concat([all_data, add_data], axis=1)
    else:
        #add_data + all_data
        all_data = concat([add_data, all_data], axis=1)
    return all_data

def collapse_frame_to_file(data, new_file, datatype=0):
    """uses pandas to collapse DataFrame back to csv/excel file"""
    try:
        if datatype in [1, 2]:
            name = new_file.with_suffix('.xlsx')
            data.to_excel(name, engine='xlsxwriter')
        if datatype in [0, 3]:
            name = new_file.with_suffix('.csv')
            data.to_csv(name, index=False, compression=None,
                        float_format='%.16g')
    except PermissionError:
        exception_raised = True
        print('A PermissionError has occured. Ensure file locations are accesible')
        return error

def individual(folder, datatype, file, out_dir, batch, default, data):
    """create newly named indivual csv files for the processed data"""
    if datatype in [0, 2, 3]:   
        new_name = file.stem + '_cut'
        if default and not batch:
            temp_out_dir = folder
        elif batch:
            temp_out_dir = out_dir / folder.name
        else:
            temp_out_dir = out_dir
        new_file_path = temp_out_dir / new_name
        error = collapse_frame_to_file(data, new_file_path, datatype)
        #potential to prompt error message
    else:
        pass


def edit_files(folder=in_folder, datatype=0, files=None, labels=None,
               out_dir=out_folder, batch=False):
    """executes editing process"""
    default = is_default(folder, datatype)
    frames = {'data': [], 'labels': []}
    for file, label in zip(files, labels):
        #transform into data frame
        data = frame_up_csv(file)
        #perform data validity checks
        if data.empty or label == 'x':
            continue
        #execute editing of valid data
        data = edit_data_frame(data, label)
        #collect edited DataFrames in List
        frames['data'].append(data)
        frames['labels'].append(label)
        #create cut versions of all files included in processing
        individual(folder, datatype, file, out_dir, batch, default, data)
    return frames